---?color=linear-gradient(to right, #c02425, #f0cb35)

@title[Docker Filesystem]

@snap[north-west]
#### Docker Filesystem
@snapend

@snap[west text-white span-100]
</br>
<ul class="split-screen-list">
    <li>Docker uses ufs (union filesystem)</li>
    <li>layered tarfiles</li>
</ul>
@snapend
