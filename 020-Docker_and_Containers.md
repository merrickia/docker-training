---?color=linear-gradient(to right, #c9a586, #553739) 
@title[Schedule]

@snap[north-west]
#### Docker and Containers
@snapend

@snap[west text-white span-100]
</br>
<ul class="split-screen-list">
    <li>Linux Virtual Environment</li>
    <li>Isolated - no OS bloat</li>
    <li>Reproducible</li>
    <li>Portable</li>
    <li>Configurable</li>
    <li>Open Source</li>
</ul>
@snapend