# docker-training


https://gitpitch.com/merrickia/docker-training/master?grs=gitlab#/


## Setup 


Use your Uni credentials to log in to 

...................


## What are containers

 * Containers are virtual environments
 * Similar to Virtual Machines (VMs), except containers virtualize the kernel; VMs virtualize hardware.
 * Faster than a VM
 * Portable
 * Lightweight
 * Repeatable

### What about docker?

 * Docker is an implementation of Containers (there are others)
 * Docker is the most widespread container technology because of its ease of use (in my opinion)
 * Docker ***images*** can be stored in docker registries (accessible over http/s)
   * https://hub.docker.com
   * https://quay.io
   * https://bigr.bios.cf.ac.uk 
   * .. plus many more 
 * Use Docker Engine (installed on your computer) to pull (download) images and run them as ***containers***.

### Why should you care?

 * Containers can be useful for scientists:
   * Capturing and sharing complicated sets of binaries/applications
   * Maintaining the same development environment across multiple computers
   * providing reproducible environments to fellow scientists


### Installing Docker Engine


https://docs.docker.com/install/


```
sudo yum install -y yum-utils   device-mapper-persistent-data   lvm2
sudo yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io
sudo systemctl start docker
sudo systemctl enable docker
```

## Containers from first-principles (tutor commands)

### chroot jails

```
mkdir -p /tmp/virtual
#
mkdir -p /tmp/virtual/proc/
mkdir -p /tmp/virtual/dev
mkdir -p /tmp/virtual/sys
#
mount -o bind /proc/ /tmp/virtual/proc/
mount -o bind /dev/ /tmp/virtual/dev
mount -o bind /sys/ /tmp/virtual/sys
#
rpm --root /tmp/virtual/ --initdb
yum --releasever=/ --installroot=/tmp/virtual install -y centos-release
yum --installroot=/tmp/virtual -y groupinstall Base
yum --installroot=/tmp/virtual -y install kernel
#
#
#
chroot /tmp/virtual
## You're now inside the chroot jail
exit
## You're now back on the host OS
```

```
sudo tar cvzf /home/centos/virtual.tar.gz -C /tmp/virtual/ .
docker import virtual.tar.gz
docker images
docker image tag ... centos-from-chroot
docker run --rm --name chroot-test-build -it centos-from-chrootl bash
```




## class commands





