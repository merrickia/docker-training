---?color=linear-gradient(to right, #918ef4, #306bac) 
@title[Running Docker Containers]

@snap[north-west]
#### Running Docker Containers
@snapend

@snap[west text-white span-100]
</br>
<ul class="split-screen-list">
    <li>docker pull/run/exec</li>
    <li>mounting host folders inside the container</li>
    <li>linking containers</li>
    <ul>
      <li>--link</li>
      <li>networks</li>
    </ul>
    <li>binding host ports to containers</li>
</ul>
@snapend