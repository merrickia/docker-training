---?color=linear-gradient(to right, #918ef4, #306bac) 
@title[Schedule]

@snap[north-west]
#### Course Schedule...
@snapend

@snap[west text-white span-100]
</br>
<ul class="split-screen-list">
    <li>Hour #1</li>
    <ul>
      <li>Setting up the class environment</li>
      <li>Docker and Containers</li>
      <li>Containers - first principles (example)</li>
      <li>The Docker filesystem</li>
      <li>Running your first Docker Container (guided examples)</li>
    </ul>
    <li>Hour #2</li>
    <ul>
      <li>Build your own Docker images</li>
    </ul>
</ul>
@snapend
