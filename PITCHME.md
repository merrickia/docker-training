---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Creating Linux Playgrounds for taught courses with Kubernetes]

### Docker Training - <br>*Biocomputing Hub*

@snap[south-west template-note text-black]
@size[0.5em](Ian Merrick</br>School of Biosciences<br/>Cardiff University)
@snapend


---?include=010-Schedule.md

---?include=020-Docker_and_Containers.md

---?include=030-Containers-First_Principles.md

---?include=040-Docker-Filesystem.md

---?include=050-Running_Docker_Containers.md

---?include=070-thanks.md
